<?php namespace Mesantara\Event\Models;

use Model;

/**
 * Event Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mesantara_event_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $dates = ['start'];

    protected $jsonable = ['extra_information'];

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
        'slug' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i'],
        'description' => 'required',
        'start' => ['required', 'date'],
        'poster' => ['required'],
    ];

    /**
     * @var array Relations
     */

    public $attachOne = [
        'poster' => ['System\Models\File']
    ];


    public function scopeIsDone($query)
    {
        return $query
            ->whereNotNull('is_done')
            ->where('is_done', true);
    }

    public function scopeIspublished($query)
    {
        return $query
            ->whereNotNull('published')
            ->where('published', true);
    }

    /**
     * Sets the "url" attribute with a URL to this object
     * @param string $pageName
     * @param Cms\Classes\Controller $controller
     */
    public function setUrl($pageName, $controller)
    {
        $params = [
            'id' => $this->id,
            'slug' => $this->slug,
        ];

        return $this->url = $controller->pageUrl($pageName, $params);
    }

}
