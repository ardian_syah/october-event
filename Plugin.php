<?php namespace Mesantara\Event;

use System\Classes\PluginBase;
use Backend;

/**
 * event Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'event',
            'description' => 'No description provided yet...',
            'author'      => 'mesantara',
            'icon'        => 'icon-calendar'
        ];
    }

    public function registerNavigation()
    {
        return [
            'event' => [
                'label' => 'Kegiatan',
                'url' => Backend::url('mesantara/event/events'),
                'icon' => 'icon-calendar',
                // 'order' => 100
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Mesantara\Event\Components\Events' => 'events',
            'Mesantara\Event\Components\Event' => 'event',
        ];
    }

}
