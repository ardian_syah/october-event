<?php namespace Mesantara\Event\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Mesantara\Event\Models\Event as EventModel;

class Events extends ComponentBase
{

    public $eventPage;

    public $events;

    public function componentDetails()
    {
        return [
            'name'        => 'All Events Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'maxItems' => [
                 'title'             => 'Max items',
                 'description'       => 'The most amount of event allowed',
                 'default'           => 10,
                 'type'              => 'string',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ],
            'eventPage' => [
                'title'       => 'Event page',
                'description' => 'Displays a detail event on the page.',
                'type'        => 'dropdown',
                // 'default'     => 'blog/post',
                'group'       => 'Links',
            ],
        ];
    }

    public function getEventPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->prepareVars();
    }

    public function prepareVars()
    {

        $this->eventPage = $this->page['eventPage'] = $this->property('eventPage');

        $this->events = $this->page['events'] = $this->listEvents();

    }

    protected function listEvents()
    {

        $events = EventModel::orderBy('start', 'desc')->take($this->property('maxItems'))->get();

        /*
         * Add a "url" helper attribute for linking to each post and category
         */
         $events->each(function($event){

             $event->setUrl($this->eventPage, $this->controller);

        });

        return $events;
    }

}
