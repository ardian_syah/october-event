<?php namespace Mesantara\Event\Components;

use Cms\Classes\ComponentBase;

class NewEvent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'NewEvent Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'maxItems' => [
                 'title'             => 'Max items',
                 'description'       => 'The most amount of event allowed',
                 'default'           => 10,
                 'type'              => 'string',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ]
        ];
    }



}
