<?php namespace Mesantara\Event\Components;

use Cms\Classes\ComponentBase;
use Mesantara\Event\Models\Event as EventModel;

class Event extends ComponentBase
{
    public $event;

    public function componentDetails()
    {
        return [
            'name'        => 'Single Event Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'Event Slug',
                'description' => 'Define Slug event for show on page',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
        ];
    }

    public function onRun()
    {
        $this->event = $this->page['event'] = $this->loadEvent();
    }

    public function loadEvent()
    {
        $slug = $this->property('slug');
        $event = EventModel::where('slug', '=', $slug)->first();

        return $event;
    }

}
