<?php namespace Mesantara\Event\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Events Back-end Controller
 */
class Events extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $bodyClass = 'compact-container';
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Mesantara.Event', 'event', 'events');

        // $this->addCss('/plugins/mesantara/event/assets/css/style.css');
    }
}
